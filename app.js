var express = require('express')
  , routes = require('./routes');

var app = module.exports = express.createServer();

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser()); // middleware that handles form posting
  app.use(express.methodOverride()); //hmm?
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

app.get('/', routes.index);

app.listen(3000);
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);


var io = require('socket.io').listen(app);

var users = {};

io.sockets.on('connection', function(socket){
    socket.on("user_hello", function(nickname){
        socket.nickname = nickname;

        users[socket.id] = {
            nickname : nickname,
        };

        io.sockets.emit("chat_message", "The Server", nickname + " joined the chat.");

        io.sockets.emit("users_list", users);
    });

    socket.on("disconnect", function(){
        nickname = socket.nickname
        delete users[socket.id];

        io.sockets.emit("users_list", users);

        socket.broadcast.emit("chat_message", "The Server", nickname + " left the chat.");
    });

    socket.on("chat_message", function(data){
      io.sockets.emit("chat_message", socket.nickname, data);
    });
});
