var socket,
    $ = AJS.$;

function connectUser(nickname){
    var socket = io.connect("http://localhost:3000");

    socket.on("connect", function(){
      $('#chat').show();
      $('#login').hide();
      socket.emit("user_hello", nickname);
    });

    socket.on("message", function(data){
      console.log(data);
    });

    socket.on("chat_message", function(user, data){
      var messages = AJS.$("#messages");
      if (user == "The Server") {
        messages.append("<div class='message server-message'><span class='message_from aui-lozenge aui-lozenge-subtle'>"+user+"</span><span class='message_body'>"+data+"</span></div>");
      } else {
        messages.append("<div class='message'><span class='message_from aui-lozenge'>"+user+"</span><span class='message_body'>"+data+"</span></div>");
      }
      $("#messages-wrap").scrollTop(messages.outerHeight()); // keep latest message visible at bottom
    });

    socket.on("users_list", function(users){
        $('#users').empty();
        $.each(users, function(socket_id, user){
          console.log('User: '+ socket_id + '-' + user.nickname);
          $('#users').append("<li>" + user.nickname + "</li>");
        });
    });

    $('#send_message').click(function() {
        var message = $('#message').val();
        $('#message').val('');
        socket.emit('chat_message', message);
    });
}

$(function(){
    $('#connect').click(function(){
        var nickname = $('#nickname').val();
        // because someone is bound to try it ;)
        if ( nickname == "The Server") {
          window.location.href = 'http://www.youtube.com/watch?v=dQw4w9WgXcQ&t=0m43s';
        } else {
          connectUser(nickname);
        }
    });

    $('#message').keypress(function(e) {
        if(e.which == 13) {
            $('#send_message').click();
        }
    });
});
